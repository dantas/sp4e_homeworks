import numpy as np
import scipy.sparse.linalg as spla


def gmres(A, b, max_iter):
    """Returns the solution x of the linear system Ax=b using Generalized
    Minimal RESidual method.

    Keyword arguments:
    A -- left hand side square matrix A (ndarray)
    b -- right hand side vector b (ndarray)
    x0 -- initial solution vector (ndarray)
    max_iter -- max number of iteration (int)
    """

    assert A.shape[0] == A.shape[1]
    assert len(b) == A.shape[0]

    m = A.shape[0]
    n = min(max_iter, m)

    # build necessary arrays
    x0 = np.zeros(b.shape)
    r = b - np.einsum("ij,j", A, x0)
    H = np.zeros((n + 1, n))
    Q = np.zeros((m, n + 1))
    Q[:, 0] = r / np.linalg.norm(r)
    x_hist = np.zeros((m, n))

    for k in range(n):

        # Arnoldi iteration
        q = np.einsum("ij,j", A, Q[:, k])
        for j in range(k + 1):
            H[j, k] = np.einsum("i,i", q, Q[:, j])
            q = q - H[j, k] * Q[:, j]
        H[k + 1, k] = np.linalg.norm(q)
        Q[:, k + 1] = q / H[k + 1, k]

        # least square problem
        beta = np.zeros(n + 1)
        beta[0] = np.linalg.norm(r)
        y = np.linalg.lstsq(H, beta, rcond=None)[0]

        # accumulate result
        x_hist[:, k] = x0 + np.einsum("ij,j", Q[:, :-1], y)

    return x_hist


if __name__ == "__main__":

    A = np.array([[8, 1], [1, 3]])
    b = np.array([2, 4])

    print("A =", A, "\n")
    print("b =", b, "\n")

    x = gmres(A, b, 2)
    print("Homework GMRES implementation:")
    print(x[:, -1], "\n")

    x = spla.gmres(A, b)
    print("Scipy GMRES implementation:")
    print(x, "\n")
