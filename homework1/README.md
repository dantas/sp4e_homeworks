# Homework 1

In this homework, we provide a Python routine to solve a quadratic function S(x) whose minimizer is the solution of the linear system Ax=b.
The homework is contained in the directory **homework01** and composed of 3 files:

- `optimizer.py`: minimization function using Scipy and plotting function
- `GMRES.py`: implementation of GMRES method 
- `main.py`: main file that can be called to solve a linear system defined in argument

The user has to source the setup file to install the required libraries and use the virtual environment:
```bash
source setup.sh
```

## Usage

The `GMRES.py` and `optimizer.py` files can be imported and used as follows:
```python
import GMRES
import optimizer
import numpy as np

# define the problem
A = np.array([[8, 1], [1, 3]])  # left hand side matrix
b = np.array([2, 4])  # right hand side vector
def S(x, A, b): return 0.5*np.dot(np.matmul(A, x), x) - np.dot(x, b)  # quadratic function to minimize
func = lambda x: S(x, A, b)

# Homework GMRES implementation
# return x solution of the linear system Ax=b for each iteration of GMRES
x = GMRES.gmres(A, b, max_iter)
# print final result
print(x[:, -1])

# Using Scipy optimization routines
method = "BFGS"
# Necessary global variables for Scipy callback
iterations = 0
result_iteration = []
x = optimizer.Minimizer(func, A, b, method)
# print final result
print(x[:, -1])

# Plot iterations
title("BFGS plot")
optimizer.PlotEachIteration(x, S, A, b, title)
```

The `main.py` file can be used as follows:
```bash
python3 main.py 8 1 1 3 2 4 BFGS --plot
```
In this example line, the first 4 arguments are the coefficients of the matrix A, the 2 following the coefficients of the vector b and the next is a method to use as minimizer. The optional argument `--plot` can be used to plot the function S(x) along with the solution at every iterations.

The `main.py` file takes arguments to build a 2-by-2 linear system but the functions in `optimizer.py` and `GMRES.py` should work also with larger systems.


