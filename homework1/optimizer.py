import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize
from scipy.sparse.linalg import lgmres


iteration = 0
result_iteration = []


def SaveResultEachInteration(result_i):
    """using this function as callback for the Minimizer() returns the result for each iteration"""
    global iteration
    global result_iteration
    result_iteration.append(result_i)
    iteration += 1


def Minimizer(function_to_optimize, A, b, method):
    """Returns an array x for each iteration step in the minimization a function s(x) by two scipy routines:\n
    Arguments: \n
    - method -- choose BFGS for minimization using  scipy.optimize.minimize and BFGS method; \n
    - method -- choose LGMRES for solving Ax=b using scipy.sparse.linalg.lgmres using LGMRES algorithm.
    """

    if method == "BFGS":
        x_min_bfgs = minimize(
            fun=function_to_optimize,
            x0=[0.0, 0.0],
            method="BFGS",
            tol=1e-9,
            callback=SaveResultEachInteration,
        ).x
        return np.stack(result_iteration, axis=1)

    if method == "LGMRES":
        x_min_lgmres = lgmres(
            A=A, b=b, x0=[0.0, 0.0], callback=SaveResultEachInteration, atol=1e-9
        )[0]
        return np.stack(result_iteration, axis=1)


def PlotEachIteration(result_iteration_method, s, A, b, title):

    fig = plt.figure()
    ax = fig.add_subplot(projection="3d")

    ax.set_xlabel("x0")
    ax.set_ylabel("x1")
    ax.set_zlabel("s(x)")
    ax.set_title(title)

    x = np.zeros(len(result_iteration_method))
    y = np.zeros(len(result_iteration_method))
    z = np.zeros(len(result_iteration_method))

    for i in range(len(result_iteration_method)):

        x[i] = result_iteration_method[0, i]
        y[i] = result_iteration_method[1, i]
        z[i] = s(result_iteration_method[:, i], A, b)

    x_surf = np.outer(np.linspace(-3, 3, 50), np.ones(50))
    y_surf = x_surf.copy().T
    z_surf = np.array(
        [[s([xi, yj], A, b) for yj in y_surf[0, :]] for xi in x_surf[:, 0]]
    )

    ax.scatter3D(x, y, z)
    ax.plot3D(x, y, z)
    ax.plot_surface(x_surf, y_surf, z_surf, cmap=plt.get_cmap("RdYlBu"), alpha=0.5)
    ax.contour(x_surf, y_surf, z_surf, cmap=plt.get_cmap("RdYlBu"))

    plt.show()


# Definiting the function to optimize
if __name__ == "__main__":

    def s(x, A, b):
        return 0.5 * np.dot(np.matmul(A, x), x) - np.dot(x, b)

    A = np.array([[8, 1], [1, 3]])
    b = np.array([2, 4])
    func = lambda x: s(x, A, b)

    iteration = 0
    result_iteration = []
    result_iteration_lgmres = Minimizer(func, A, b, method="LGMRES")
    print("Result each iteraction LGMES", result_iteration_lgmres)

    iteration = 0
    result_iteration = []
    result_iteration_bfgs = Minimizer(func, A, b, method="BFGS")
    print("Result each iteraction BFGS", result_iteration_bfgs)

    PlotEachIteration(result_iteration_lgmres, s, A, b, "LGMRES")
    PlotEachIteration(result_iteration_bfgs, s, A, b, "BFGS")
