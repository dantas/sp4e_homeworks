import numpy as np
import argparse
import GMRES
import optimizer


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("A", type=float, action="store", nargs=4, help="Left hand side matrix A")
    parser.add_argument("b", type=float, action="store", nargs=2, help="Right hand side vector b")
    parser.add_argument("minimizer", type=str, help="Type of minimizer to be used", choices=["GMRES", "BFGS", "LGMRES"])
    parser.add_argument("-p", "--plot", action="store_true", help="Plot iterations for the chosen minimizer")
    args = parser.parse_args()

    print("Minimizers:", args.minimizer)

    # Build A and b arrays from args
    A = np.array(args.A).reshape((2, 2))
    b = np.array(args.b).reshape(2)

    def s(x, A, b):
        return 0.5 * np.dot(np.matmul(A, x), x) - np.dot(x, b)
    func = lambda x: s(x, A, b)

    print("A =", A, "\n")
    print("b =", b, "\n")

    # use the chosen minimizer
    mini = args.minimizer
    iteration = 0
    result_iteration = []
    if mini == "BFGS":
        print("Using BFGS")

        x = optimizer.Minimizer(func, A, b, mini)
        print("x =", x[:, -1])

    elif mini == "LGMRES":
        print("Using LGMRES")

        x = optimizer.Minimizer(func, A, b, mini)
        print("x =", x[:, -1])

    else:
        print("Using homework GMRES:")
        x = GMRES.gmres(A, b, 2)
        print("x =", x[:, -1])

    # plot if asked
    if args.plot:
        optimizer.PlotEachIteration(x, s, A, b, mini)
