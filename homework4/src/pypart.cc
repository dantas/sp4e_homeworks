#include <pybind11/cast.h>
#include <pybind11/detail/common.h>
#include <pybind11/pybind11.h>

#include <memory>

#include "system_evolution.hh"

namespace py = pybind11;

#include "compute.hh"
#include "compute_gravity.hh"
#include "compute_interaction.hh"
#include "compute_temperature.hh"
#include "compute_verlet_integration.hh"
#include "csv_writer.hh"
#include "material_points_factory.hh"
#include "particles_factory_interface.hh"
#include "ping_pong_balls_factory.hh"
#include "planets_factory.hh"

PYBIND11_MODULE(pypart, m) {
  m.doc() = "pybind of the Particles project";

  // bind the routines here

  // Factories
  py::class_<ParticlesFactoryInterface>(m, "ParticlesFactoryInterface")
      .def("getInstance", &ParticlesFactoryInterface::getInstance,
           py::return_value_policy::reference)
      .def("createSimulation",
           py::overload_cast<const std::string&, Real, py::function>(
               &ParticlesFactoryInterface::createSimulation<py::function>),
           py::arg("fname"), py::arg("timestep"), py::arg("func"),
           py::return_value_policy::reference)
      .def("createParticle", &ParticlesFactoryInterface::createParticle)
      .def_property("system_evolution",
                    &ParticlesFactoryInterface::getSystemEvolution, nullptr);

  py::class_<MaterialPointsFactory, ParticlesFactoryInterface>(
      m, "MaterialPointsFactory")
      .def("getInstance", &MaterialPointsFactory::getInstance,
           py::return_value_policy::reference)
      .def("createDefaultComputes",
           &MaterialPointsFactory::createDefaultComputes, py::arg("timestep"));

  py::class_<PlanetsFactory, ParticlesFactoryInterface>(m, "PlanetsFactory")
      .def("getInstance", &PlanetsFactory::getInstance,
           py::return_value_policy::reference);

  py::class_<PingPongBallsFactory, ParticlesFactoryInterface>(
      m, "PingPongBallsFactory")
      .def("getInstance", &PingPongBallsFactory::getInstance,
           py::return_value_policy::reference);

  // System evolution
  py::class_<SystemEvolution>(m, "SystemEvolution")
      .def("evolve", &SystemEvolution::evolve)
      .def("addCompute", &SystemEvolution::addCompute, py::arg("compute"))
      .def("setNSteps", &SystemEvolution::setNSteps, py::arg("nsteps"))
      .def("setDumpFreq", &SystemEvolution::setDumpFreq, py::arg("freq"))
      .def("getSystem", &SystemEvolution::getSystem);

  // System
  py::class_<System>(m, "System");

  // Compute
  py::class_<Compute, std::shared_ptr<Compute>>(m, "Compute")
      .def("compute", &Compute::compute, py::arg("system"));
  // CSV Writer
  py::class_<CsvWriter, Compute, std::shared_ptr<CsvWriter>>(m, "CsvWriter")
      .def(py::init<const std::string&>(), py::arg("filename"))
      .def("write", &CsvWriter::write, py::arg("system"));

  // ComputeInteraction
  py::class_<ComputeInteraction, Compute, std::shared_ptr<ComputeInteraction>>(
      m, "ComputeInteraction")
      .def("applyOnPairs", &ComputeInteraction::applyOnPairs<py::function>,
           py::arg("func"), py::arg("system"));

  // ComputeGravity
  py::class_<ComputeGravity, ComputeInteraction,
             std::shared_ptr<ComputeGravity>>(m, "ComputeGravity")
      .def(py::init<>())
      .def("compute", &ComputeGravity::compute, py::arg("system"))
      .def("setG", &ComputeGravity::setG, py::arg("G"));

  // ComputeTemperature
  py::class_<ComputeTemperature, Compute, std::shared_ptr<ComputeTemperature>>(
      m, "ComputeTemperature")
      .def("compute", &ComputeTemperature::compute, py::arg("system"))
      .def("getConductivity", &ComputeTemperature::getConductivity)
      .def("getCapacity", &ComputeTemperature::getCapacity)
      .def("getDensity", &ComputeTemperature::getDensity)
      .def("getL", &ComputeTemperature::getL)
      .def("getDeltat", &ComputeTemperature::getDeltat);

  // ComputeVerletIntegration
  py::class_<ComputeVerletIntegration, Compute,
             std::shared_ptr<ComputeVerletIntegration>>(
      m, "ComputeVerletIntegration")
      .def(py::init<Real&>(), py::arg("timestep"))
      .def("setDeltaT", &ComputeVerletIntegration::setDeltaT, py::arg("dt"))
      .def("compute", &ComputeVerletIntegration::compute, py::arg("system"))
      .def("addInteraction", &ComputeVerletIntegration::addInteraction,
           py::arg("interaction"));
}
