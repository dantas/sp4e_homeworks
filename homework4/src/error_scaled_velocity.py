import pandas as pd
import main
import argparse
import compute_error


def generateInput(scale, planet_name, input_filename, output_filename):
    """Generate an input file from a given input but scaling velocity of a given planet.\n
    Arguments: \n
    scale -- scale factor to be applied to the velocity."""

    df = pd.read_csv(input_filename, sep=" ", index_col=10)
    df.loc[planet_name].VX *= scale
    df.loc[planet_name].VY *= scale
    df.loc[planet_name].VZ *= scale
    df.name = df.index
    df.to_csv(output_filename, sep=" ", index=False)

    return output_filename


def launchParticles(input_file, nb_steps, freq, timestep):
    """Launch the code given an input.\n
    Arguments: \n
    input -- input given by generateInput(); \n
    nb_steps -- number of time-steps; \n
    freq -- frequency for dumps."""

    main.main(nb_steps, freq, input_file, "planet", timestep)

    return


def runAndComputeError(scale, planet_name, input_file, nb_steps, freq, timestep):
    """Arguments: \n
    scale -- scale factor to be applied to the velocity;\n
    planet name \n
    input -- input file (.csv);\n
    nb_steps -- number of time-steps; \n
    freq -- frequency for dumps."""

    # Run the Particles code with the provided input
    launchParticles(input_file, nb_steps, freq, timestep)
    pos_ref = compute_error.readPositions(planet_name, "trajectories")

    # Run the particle code with a scaled input
    generateInput(scale, planet_name, input_file, "input_scaled.csv")
    launchParticles("input_scaled.csv", nb_steps, freq, timestep)
    pos = compute_error.readPositions(planet_name, "dumps")

    # Compute error between the simulations
    error = compute_error.computeError(pos, pos_ref)
    print(planet_name, error)

    return error


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Launch particles code")

    parser.add_argument(
        "scale_factor",
        type=float,
        help="specify a scale factor to be applied on the velocity",
    )
    parser.add_argument("planet_name", type=str, help="specify a planet")
    parser.add_argument("input_filename", type=str, help="start/input filename (.csv)")

    args = parser.parse_args()
    scale = args.scale_factor
    planet_name = args.planet_name
    input_filename = args.input_filename
    nb_steps = 365
    freq = 1
    timestep = 1

    runAndComputeError(scale, planet_name, input_filename, nb_steps, freq, timestep)
