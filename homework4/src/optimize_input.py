import scipy as sc
import numpy as np
import matplotlib.pyplot as plt
from error_scaled_velocity import runAndComputeError


planet_name = "mercury"
input_file = "init.csv"
nb_steps = 365
freq = 1
timestep = 1


def optimize():
    x0 = 1

    minimum = sc.optimize.fmin(
        runAndComputeError,
        x0,
        args=(planet_name, input_file, nb_steps, freq, timestep),
        full_output=True,
        retall=True,
    )
    return minimum


def plotError(minimum):
    scalings = minimum[-1]
    errors = np.zeros(len(scalings))
    for i, scaling in enumerate(scalings):
        errors[i] = runAndComputeError(
            scaling, planet_name, input_file, nb_steps, freq, timestep
        )
    plt.figure()
    plt.title("Optimization errors")
    plt.xlabel("Scaling factor")
    plt.ylabel("Error")
    plt.semilogy(scalings, errors, color="r", linestyle="--", marker="+")
    plt.show()


if __name__ == "__main__":
    minimum = optimize()
    plotError(minimum)
