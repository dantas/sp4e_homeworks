import numpy as np
import os


def readPositions(planet_name, directory):
    file_list = os.listdir(directory)
    file_list.sort()
    positions = np.zeros((len(file_list), 3))
    for i, f in enumerate(file_list):
        file = os.path.join(directory, f)
        tmp_pos = np.loadtxt(file, usecols=(0, 1, 2))
        names = np.loadtxt(file, usecols=-1, dtype=str)
        positions[i, :] = tmp_pos[np.argwhere(names == planet_name)]
    return positions


def computeError(positions, positions_ref):
    diff = positions[:-1, :] - positions_ref[1:, :]
    return np.linalg.norm(diff)


if __name__ == "__main__":

    names = [
        "sun",
        "mercury",
        "neptune",
        "jupiter",
        "uranus",
        "mars",
        "earth",
        "venus",
        "saturn",
    ]
    for name in names:
        pos_ref = readPositions(name, "trajectories")
        pos = readPositions(name, "dumps")
        print(name, computeError(pos, pos_ref))
