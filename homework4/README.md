# Homework 4

In this homework, we add a python binding for the C++ particle code. 

The homework is contained in the directory **homework4** and we add / change 4 files to the particles code:

- `pypart.cc`: implementation of the binding
- `main.py`: routine to run a simulation of the particles code using the binding.
- `compute_error.py`: compute the error between two simlations for planet particle type
- `error_scaled_velocity.py`: scale the velocity of a given input, launch the simualtion, and compute error between the scaled input simulaition results and non scaled input ones for a planet particle type.
- `optimize_input.py`: optimize the scaling factor for the velocity of Mercury so that the error is minimized and plot the error vs scaling factor

The user has to source the setup file to install the required libraries and use the virtual environment to run the python files:
```bash
source setup.sh
```

## Usage

To run a simulation of the particle code using python the user has to:

First Compile and Build:
```bash
cd homework4
mkdir build
cd build
ccmake ..
make
```
The compiled files will then be inside `build/src/`. 

In a  py file:
```python
import pypart
```

The `main.py` file can be used as follows:
```bash
python3 main.py nsteps dump_freq input.csv particle_type timestep
```
with:
* `nsteps` (int): the number of simulation step
* `dump_freq` (int): the frequency at which to dump output
* `input.csv` (string): the input file containing the starting state of the particles
* `particle_type` (string): the type of particle to simulate
* `timestep` (float): the timestep of the simulation

For example:
```bash
python3 main.py 100 10 init.csv planet 1
```

The `error_scaled_velocity.py` can be used as follows:
```bash
python3 error_scaled_velocity.py scale_factor planet_name init.csv 
```
with:
* `scale_factor` (float): a scalar value that is going to be applied to the velocity input of a given planet
* `planet_name` (string): the name of a chosen planet
* `input.csv` (string): the input file containing the starting state of the particles

For example:
```bash
python3 error_scaled_velocity.py 100 earth init.csv 
```

The file `optimize_input.py` can be launched with Python and doesn't take any argument:
```bash
python3 optimize_input.py
```
