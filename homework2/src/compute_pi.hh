#ifndef COMPUTE_PI_HH
#define COMPUTE_PI_HH

#include "series.hh"

class ComputePi : public Series {
public:
  ComputePi();

  ~ComputePi();

  Real compute(UInt N) override;

  virtual Real getAnalyticPrediction() const override;

  virtual Real computeTerm(UInt k) const override;

  virtual Real getCurrentValue() const override;
};

#endif
