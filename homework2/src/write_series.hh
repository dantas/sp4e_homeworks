#ifndef WRITE_SERIES_HPP
#define WRITE_SERIES_HPP

#include "dumper_series.hh"

#include <cmath>
#include <fstream>
#include <ostream>

class WriteSeries : public DumperSeries {
public:
  WriteSeries(Series & series, UInt const & maxiter);

  ~WriteSeries();

  virtual void dump(std::ostream & os) const override;

  void dump(std::string const & filename) const;

  void setSeparator(char const & separator);

private:
  char separator;
  std::string file_extension;
};

#endif
