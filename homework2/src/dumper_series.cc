#include "dumper_series.hh"
#include <iostream>

DumperSeries::DumperSeries(Series & series, UInt const & maxiter)
    : series(series), maxiter(maxiter), precision(5) {}

DumperSeries::~DumperSeries() {}

void DumperSeries::setPrecision(const UInt &precision) {
  this->precision = precision;  
}
