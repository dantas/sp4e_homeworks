import numpy as np
import argparse
import os
import matplotlib.pyplot as plt


def parseFile(file, ext):
    exts = np.array([".txt", ".csv", ".psv"])
    delims = np.array([' ', ',', '|'])
    delim = delims[np.where(exts == ext)][0]
    print(delim)
    k, values = np.loadtxt(file, delimiter=delim, skiprows=2, unpack=True)
    conv = np.loadtxt(file, delimiter=delim, max_rows=1, usecols=1)
    return k, values, conv


def plotSeries(title, k, values, conv):
    plt.figure()
    plt.title(title)
    plt.xlabel("k")
    plt.ylabel("Value S(k)")
    plt.plot(k, values, label="S(k)", marker="o")
    if conv:
        plt.hlines(conv, 0, len(values) - 1, color="r", label="Converges to")
    plt.legend()
    plt.show()


if __name__ == "__main__":
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("file", type=str)
    args = argument_parser.parse_args()

    ext = os.path.splitext(args.file)[1]
    if ext not in [".txt", ".csv", ".psv"]:
        raise Exception("Unsupported file extension: should be .txt, .csv or .psv")

    k, values, conv = parseFile(args.file, ext)
    title = os.path.splitext(args.file)[0]
    plotSeries(title, k, values, conv)
