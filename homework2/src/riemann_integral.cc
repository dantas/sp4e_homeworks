#include "riemann_integral.hh"
#include "series.hh"
#include <cmath>
#include <iostream>

RiemannIntegral::RiemannIntegral(MathFunction const f, Real const a,
                                 Real const b)
    : Series(), f(f), a(a), b(b), dx(0) {}

RiemannIntegral::~RiemannIntegral() {}

Real RiemannIntegral::compute(UInt N) {
  this->dx = (this->b - this->a) / double(N);
  Real result = Series::compute(N);
  this->current_index = 0;
  this->current_value = 0.;
  return result;
}

Real RiemannIntegral::computeTerm(UInt k) const {
  Real x = this->a + k * this->dx;
  Real term;
  switch (this->f) {
  case MathFunction::_cube:
    term = std::pow(x, 3);
    break;
  case MathFunction::_cos:
    term = std::cos(x);
    break;
  case MathFunction::_sin:
    term = std::sin(x);
    break;
  }
  return term * this->dx;
}

Real RiemannIntegral::getAnalyticPrediction() const {
  Real result;
  switch (this->f) {
  case MathFunction::_cube:
    result = 0.25 * (std::pow(this->b, 4) - std::pow(this->a, 4));
    break;
  case MathFunction::_cos:
    result = std::sin(this->b) - std::sin(this->a);
    break;
  case MathFunction::_sin:
    result = std::cos(this->a) - std::cos(this->b);
    break;
  }
  return result;
}
