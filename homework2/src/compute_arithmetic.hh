#ifndef COMPUTE_ARITHMETIC_HH
#define COMPUTE_ARITHMETIC_HH

#include "series.hh"

class ComputeArithmetic : public Series {
public:
  ComputeArithmetic();

  ~ComputeArithmetic();

  virtual Real computeTerm(UInt k) const override;
};

#endif
