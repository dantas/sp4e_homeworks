#ifndef SERIES_HPP
#define SERIES_HPP

using Real = double;
using UInt = unsigned int;

class Series {
public:
  Series();

  virtual ~Series();

  virtual Real compute(UInt N);

  virtual Real getAnalyticPrediction() const;

  virtual Real computeTerm(UInt k) const = 0;

  virtual Real getCurrentValue() const;

protected:
  UInt current_index;
  Real current_value;
};

#endif
