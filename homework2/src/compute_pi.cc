#include "compute_pi.hh"
#include "series.hh"
#include <cmath>
#include <iostream>

ComputePi::ComputePi() : Series() {}

ComputePi::~ComputePi() {}

Real ComputePi::compute(UInt N) {
  return sqrt(6. * Series::compute(N));
}

Real ComputePi::getAnalyticPrediction() const { return M_PI; }

Real ComputePi::computeTerm(UInt n) const { return 1. / (n * n); }

Real ComputePi::getCurrentValue() const  { return sqrt(6. * this->current_value); }
