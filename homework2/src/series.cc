#include "series.hh"
#include <iostream>
#include <math.h>
#include <stdexcept>

Series::Series() : current_index(0), current_value(0.) {}

Series::~Series() {}

Real Series::compute(UInt N) {
  if (this->current_index <= N) {
    N -= this->current_index;
  } else {
    this->current_index = 0;
    this->current_value = 0.;
  }
  for (int n = 0; n < N; n++) {
    this->current_index += 1;
    this->current_value += computeTerm(this->current_index);
  }
  return this->current_value;
}

Real Series::getAnalyticPrediction() const { return std::nan(""); }

Real Series::getCurrentValue() const { return this->current_value; }
