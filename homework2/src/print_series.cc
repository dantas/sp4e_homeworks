#include "print_series.hh"
#include "dumper_series.hh"

#include <cmath>
#include <iomanip>
#include <iostream>
#include <ostream>

PrintSeries::PrintSeries(Series & series, const UInt & maxiter,
                         const UInt & frequency)
    : DumperSeries(series, maxiter), frequency(frequency) {}

PrintSeries::~PrintSeries() {}

void PrintSeries::dump(std::ostream & os) const {
  os.setf(std::ios::scientific);
  os.precision(this->precision);

  Real prediction = this->series.getAnalyticPrediction();
  if (!std::isnan(prediction)) {
    os << "Should converge to: " << prediction << std::endl;
  }

  os << std::right << std::setw(this->precision + 5) << "k:"
     << "   "
     << "Value:" << std::endl;
  for (int i = 0; i < this->maxiter; ++i) {
    if (i % this->frequency == 0) {
      Real value = this->series.compute(i + 1);
      os << std::right << std::setw(this->precision + 5) << i + 1 << "   "
         << value << std::endl;
    }
  }
}
