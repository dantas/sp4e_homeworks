#include "write_series.hh"
#include "dumper_series.hh"
#include <cmath>
#include <fstream>
#include <iostream>
#include <ostream>
#include <stdexcept>

WriteSeries::WriteSeries(Series & series, UInt const & maxiter)
    : DumperSeries(series, maxiter), separator(' '), file_extension(".txt") {}

WriteSeries::~WriteSeries() {}

void WriteSeries::dump(std::ostream & os) const {
  os.setf(std::ios::scientific);
  os.precision(this->precision);

  Real prediction = this->series.getAnalyticPrediction();
  os << "Analytic" << this->separator << prediction << std::endl;

  os << "k" << this->separator << "Value" << std::endl;

  for (int i = 0; i < this->maxiter; ++i) {
    os << i + 1 << this->separator << this->series.compute(i + 1) << std::endl;
  }
}

void WriteSeries::dump(std::string const & filename) const {
  std::string new_filename = filename;
  new_filename.append(this->file_extension);
  std::ofstream outfile(new_filename);

  dump(outfile);

  outfile.close();
}

void WriteSeries::setSeparator(const char & separator) {
  switch (separator) {
  case ',':
    this->file_extension = ".csv";
    break;
  case ' ':
    this->file_extension = ".txt";
    break;
  case '|':
    this->file_extension = "psv";
    break;
  default:
    throw std::runtime_error("Error: separator should be ' ', ',' or '|'.");
  }
  this->separator = separator;
}
