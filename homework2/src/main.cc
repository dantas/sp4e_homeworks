#include "compute_arithmetic.hh"
#include "compute_pi.hh"
#include "dumper_series.hh"
#include "print_series.hh"
#include "riemann_integral.hh"
#include "series.hh"
#include "write_series.hh"

#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <ostream>
#include <sstream>
#include <string>

int main(int argc, char ** argv) {

  std::stringstream sstr;
  for (int i = 1; i < argc; ++i) {
    sstr << argv[i] << " ";
  }

  std::string series_type;
  sstr >> series_type;

  // set and instanciate series
  std::unique_ptr<Series> series = nullptr;
  if (series_type == "arithmetic") {
    series = std::make_unique<ComputeArithmetic>();
  } else if (series_type == "pi") {
    series = std::make_unique<ComputePi>();
  } else if (series_type == "riemann") {
    Real a, b;
    sstr >> a >> b;

    std::string func;
    sstr >> func;

    std::map<std::string, MathFunction> func_map;
    func_map["cube"] = MathFunction::_cube;
    func_map["sin"] = MathFunction::_sin;
    func_map["cos"] = MathFunction::_cos;

    series = std::make_unique<RiemannIntegral>(func_map[func], a, b);
  } else {
    throw std::runtime_error("Error: non-defined series type. Should be "
                             "'arithmetic', 'pi' or 'riemann'");
  }

  std::string dump_type;
  sstr >> dump_type;

  UInt N;
  sstr >> N;
  
  UInt frequency = 1;
  if (dump_type == "print") {
    sstr >> frequency;
  }

  UInt precision = 5;
  if (!sstr.eof()) {
    sstr >> precision;
  }

  // set and instanciate dumper
  if (dump_type == "print") {
    PrintSeries printer(*series, N, frequency);
    printer.setPrecision(precision);
    std::cout << printer;
  } else if (dump_type == "write") {
    WriteSeries writer(*series, N);
    writer.dump(series_type);
  } else {
    throw std::runtime_error(
        "Error: non-defined dumper type. Should be 'print' or 'write'");
  }

  return 0;
}
