#ifndef RIEMANN_INTEGRAL_HPP
#define RIEMANN_INTEGRAL_HPP

#include "series.hh"

#include <cmath>

enum class MathFunction { _cube, _sin, _cos };

class RiemannIntegral : public Series {
public:
  RiemannIntegral(MathFunction const f, Real const a, Real const b);

  ~RiemannIntegral();

  Real compute(UInt N) override;

  virtual Real computeTerm(UInt k) const override;

  virtual Real getAnalyticPrediction() const override;

private:
  MathFunction f;
  Real a;
  Real b;
  Real dx;
};

#endif
