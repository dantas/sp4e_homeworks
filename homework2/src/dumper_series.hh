#ifndef DUMPER_SERIES_HPP
#define DUMPER_SERIES_HPP

#include "series.hh"
#include <ostream>

class DumperSeries {
public:
  DumperSeries(Series & series, UInt const & maxiter);

  virtual ~DumperSeries();

  virtual void dump(std::ostream & os) const = 0;

  virtual void setPrecision(UInt const & precision);

protected:
  void sendOutput();

  Series & series;

  UInt maxiter;

  UInt precision;
};

inline std::ostream & operator<<(std::ostream & stream, DumperSeries & _this) {
  _this.dump(stream);
  return stream;
}

#endif
