#ifndef PRINT_SERIES_HPP
#define PRINT_SERIES_HPP

#include "dumper_series.hh"

#include <cmath>
#include <iostream>
#include <ostream>

class PrintSeries : public DumperSeries {
public:
  PrintSeries(Series & series, UInt const & maxiter, UInt const & frequency);

  ~PrintSeries();

  // Print series value at defined frequency until maxiter
  virtual void dump(std::ostream & os = std::cout) const override;

private:
  // Print frequency for dump
  UInt frequency;
};

#endif
