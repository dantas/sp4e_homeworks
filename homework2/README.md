# Homework 2

In this homework, we provide routine in C++ to compute series and dump the results. 
The homework is contained in the directory **homework2**.

## Computation of series:


Compute the arithmetic series:

```math
    S = \sum_{k=1}^N k
```

Compute the pi series:

```math 
    \pi = \sqrt{6\sum_{k=1}^N \frac{1}{k^2}}
```

Compute Riemann integral:
```math
    \int_a^b f(x) dx = \lim_{N\to\infty}\sum_{i=1}^N f(x_i)\Delta x 
```

The declarations can be found in:

`compute_arithmetic.hh`

`compute_pi.hh`

`riemann_integral.hh`

## Series dumper:

For dump the results, the declarations can be found in:

`dumper_series.hh` 

`write_series.hh`

There is also a python script `plot_series.py` for plotting the results.

## Usage

Compile and Build
```
cd homework2
mkdir build
cd build
ccmake ..
make
```

The `main.cc` file can be used as follows:
```bash
cd build/src
./main series_type output_command number_iterations (precision)
```
Where the arguments are:

* `series_type` : write the choosen series (*aritmetic*, *pi* or *riemann*)  
For *riemann*, the boundaries *a* and *b* of the integration domain and the function *f* to integrate (*cube*, *sin* or *cos*).  
For instance: `./main riemann 0 1 cube write 100` will write the result of the integral of $`x^3`$ using 100 points.

* `output_command` : write the choosen output (*print* or *write*)  
For *print*, the print *frequency* is needed (`./main pi print 100 10` will print every 10 iteration)

* `number_iterations` : an integer number.

* `precision` : (optional) set the decimal precision


If the `output_command` is write a `series_type.txt` is created at build directory.

An example for computing a series, is provided bellow:
```bash
./main pi print 10 1 10
```
This will print the first 10 iterations of the Pi series with 10 digits of precision.

To plot the results run the `plot_series.py` as follows:
```
python3 plot_series.py file
```
Where `file` is the result file containing the values to plot. This file is obtain by using the 'write' option in the `main.cc`. It can be a txt, csv or psv file.


## Division of the project

The implementation of this project was initially divided between the two authors as follows:

Computation of Series and documentation: Raquel Dantas Batista

Dump results and Computation of Riemann Integral:  Shad Durussel

In consideration that the two implementations are related, there is the contribution of both authors in many parts of the project (e.g. main.cc, documentation, refactorization, bugFIX, etc).