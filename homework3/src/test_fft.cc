#include <gtest/gtest.h>

#include "fft.hh"
#include "matrix.hh"
#include "my_types.hh"

/*****************************************************************/
TEST(FFT, transform) {
  UInt N = 512;
  Matrix<complex> m(N);

  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = cos(k * i);
  }

  Matrix<complex> res = FFT::transform(m);

  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    if (std::abs(val) > 1e-10)
      std::cout << i << "," << j << " = " << val << std::endl;

    if (i == 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else if (i == N - 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else
      ASSERT_NEAR(std::abs(val), 0, 1e-10);
  }
}
/*****************************************************************/

TEST(FFT, inverse_transform) {
  UInt N = 512;
  Real n = (double)N;
  Matrix<complex> m(N);

  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    if (i == 1 && j == 0)
      val = n * n / 2;
    else if (i == N - 1 && j == 0)
      val = n * n / 2;
    else
      val = 0;
  }

  Matrix<complex> res = FFT::itransform(m);

  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    ASSERT_NEAR(val.real(), cos(k * i), 1e-10);
  }
}
/*****************************************************************/

TEST(FFT, compute_frequencies) {
  UInt N = 8;

  auto freqs = FFT::computeFrequencies(N);

  UInt freq_i, freq_j;
  for (auto&& entry : index(freqs)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    freq_i = i < N / 2 ? i : i - N;
    freq_j = j < N / 2 ? j : j - N;
    ASSERT_EQ(val.real(), freq_i);
    ASSERT_EQ(val.imag(), freq_j);
  }

  N = 9;

  freqs = FFT::computeFrequencies(N);

  for (auto&& entry : index(freqs)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    freq_i = i <= (N - 1) / 2 ? i : i - N;
    freq_j = j <= (N - 1) / 2 ? j : j - N;
    ASSERT_EQ(val.real(), freq_i);
    ASSERT_EQ(val.imag(), freq_j);
  }
}
