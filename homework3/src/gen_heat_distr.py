import numpy as np
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("size", type=float, help="half size of the square domain")
    parser.add_argument("radius", type=float, help="radius of heat distribution")
    parser.add_argument(
        "N", type=int, help="number of point to compute on the half square"
    )
    parser.add_argument("filename", type=str, help="name of generated input file")
    args = parser.parse_args()

    N = args.N
    distr = np.zeros((2 * N, 2 * N))

    size = args.size
    x = np.linspace(-size, size, 2 * N)
    y = np.linspace(-size, size, 2 * N)
    xx, yy, zz = np.meshgrid(x, y, 0)
    positions = np.vstack([xx.ravel(), yy.ravel(), zz.ravel()]).T

    r = args.radius
    heat = np.where(np.sum(positions**2, axis=1) < r, 1, 0).reshape((-1, 1))

    velocities = np.zeros(positions.shape)
    forces = np.zeros(positions.shape)
    masses = np.ones((positions.shape[0], 1))
    temperature = np.zeros((positions.shape[0], 1))
    heat_rates = np.zeros((positions.shape[0], 1))

    file_data = np.hstack((positions, velocities, forces, masses, temperature, heat_rates, heat))
    np.savetxt(args.filename, file_data, delimiter=" ")
