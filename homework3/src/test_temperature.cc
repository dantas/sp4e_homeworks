#include <complex>
#include <gtest/gtest.h>

#include <cmath>
#include <iostream>
#include <iterator>

#include "compute_temperature.hh"
#include "material_point.hh"
#include "material_points_factory.hh"
#include "my_types.hh"
#include "particle.hh"
#include "system.hh"

// Input data
Real L = 2.;
Real x_0 = -1.;
Real y_0 = -1.;
Real grid_size = 512;
Real nparticles = grid_size * grid_size;
Real dl = L / grid_size;
Real heat_conductivity = 1.;
Real heat_capacity = 1.;
Real mass = 1.;
Real time_increment = 1.;

// Create system and set input for tests
System createSystemTest() {
  System system;
  // Create material points
  for (UInt i = 0; i < grid_size; i++) {
    for (UInt j = 0; j < grid_size; j++) {
      // Create particle
      auto &instance = MaterialPointsFactory::getInstance();
      auto point = instance.createParticle();
      // Set particles positions
      point->getPosition()[0] = x_0 + Real(i) * dl;
      point->getPosition()[1] = y_0 + Real(j) * dl;
      // Create system
      system.addParticle(std::move(point));
    };
  };
  return system;
}

// Set input data to test ComputeTemperature
ComputeTemperature setTestInput() {
  ComputeTemperature test;
  test.setHeatCapacity(heat_capacity);
  test.setHeatConductivity(heat_conductivity);
  test.setMass(mass);
  test.setDl(dl);
  test.setTimeIncrement(time_increment);
  return test;
}

/****************************************************************/
// Test ComputeTemperature for homogeneous temperature
TEST(ComputeTemperature, homogeneous_temperature) {
  // Input for homogeneous temperature
  Real temperature_homogeneous = 20.;  // Constant value
  Real heat_source = 0.;
  auto system = createSystemTest();
  auto test = setTestInput();

  for (auto&& point : system) {
    auto &matpoint = static_cast<MaterialPoint &>(point);
    // Set the input temperature and heat source
    matpoint.getTemperature() = temperature_homogeneous;
    matpoint.getHeatSource() = heat_source;
  }

  // Compute temperature
  test.compute(system);

  // Validate test
  for (auto&& point : system) {
    auto &matpoint = static_cast<MaterialPoint &>(point);
    ASSERT_NEAR(matpoint.getTemperature(), temperature_homogeneous, 1e-9);
  }
}

/****************************************************************/

// Input for sin function temperature
Real equilib_temp(Real x) { return sin(2. * M_PI * x / L); }
Real heatsource_sinfunction(Real x) {
  return (2. * M_PI / L) * (2. * M_PI / L) * sin(2. * M_PI * x / L);
}

// Test ComputeTemperature for volumetric heat source sin
TEST(ComputeTemperature, sin_temperature) {
  auto system = createSystemTest();
  auto test = setTestInput();

  for (auto&& point : system) {
    auto &matpoint = static_cast<MaterialPoint &>(point);
    // Set the input temperature and heat source
    auto x = matpoint.getPosition()[0];
    matpoint.getTemperature() = equilib_temp(x);
    matpoint.getHeatSource() = heatsource_sinfunction(x);
  }
  // Compute temperature
  test.compute(system);
  test.compute(system);

  // Validate test
  for (auto&& point : system) {
    auto &matpoint = static_cast<MaterialPoint &>(point);
    Real x = matpoint.getPosition()[0];
    ASSERT_NEAR(matpoint.getTemperature(), equilib_temp(x), 1e-9);
  }
}

/****************************************************************/

// Input for -1_0_+1 function temperature
Real initial_temp(Real x) {
  Real temperature = x;
  if (x <= 0.5) {
    temperature = -x - 1.;
  }
  if (x >= 0.5) {
    temperature = -x + 1.;
  }
  return temperature;
}

Real heatsource_function(Real x) {
  Real small_number = 1.e-2;
  // x different of +/-0.5
  Real heat = 0.;
  // For x = -0.5
  if (std::abs(x + 0.5) < small_number) {
    heat = -1.;
  }
  // For x = +0.5
  if (std::abs(x - 0.5) < small_number) {
    heat = 1.;
  }
  return heat;
}

// Test ComputeTemperature for volumetric heat source -1_0_+1
TEST(ComputeTemperature, temperature_variation) {
  auto system = createSystemTest();
  auto test = setTestInput();

  for (auto&& point : system) {
    auto &matpoint = static_cast<MaterialPoint &>(point);
    // Set the input temperature and heat source
    auto x = matpoint.getPosition()[0];
    matpoint.getTemperature() = initial_temp(x);
    matpoint.getHeatRate() = heatsource_function(x);
  }
  // Compute temperature
  test.compute(system);

  // Validate test
  for (auto&& point : system) {
    auto &matpoint = static_cast<MaterialPoint &>(point);
    Real x = matpoint.getPosition()[0];
    ASSERT_NEAR(matpoint.getTemperature(), initial_temp(x), 1e-9);
  }
}
