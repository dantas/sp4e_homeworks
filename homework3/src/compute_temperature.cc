#include "compute_temperature.hh"

#include <cmath>
#include <iostream>

#include "fft.hh"
#include "material_point.hh"
#include "matrix.hh"
#include "my_types.hh"

/* -------------------------------------------------------------------------- */

void ComputeTemperature::compute(System &system) {
  // Total number of particles (size of the system)
  UInt grid_size = std::sqrt(system.getNbParticles());

  Matrix<complex> temperature(grid_size);
  Matrix<complex> heat_source(grid_size);

  UInt k = 0;
  // Temperature matrix
  for (UInt i = 0; i < grid_size; i++) {
    for (UInt j = 0; j < grid_size; j++) {
      Particle &point = system.getParticle(k);
      auto &matpoint = static_cast<MaterialPoint &>(point);
      temperature(i, j) = matpoint.getTemperature();
      heat_source(i, j) = matpoint.getHeatSource();
      k++;
    }
  }

  // FFT transform
  auto transform_temperature = FFT::transform(temperature);
  auto transform_heatsource = FFT::transform(heat_source);
  // FFT frequencies
  auto frequencies = FFT::computeFrequencies(grid_size);
  // Coordinates Fourier space
  const Real l = pow(2. * M_PI / (dl * grid_size), 2);
  Matrix<complex> coord_fourier(grid_size);
  for (UInt i = 0; i < grid_size; i++) {
    for (UInt j = 0; j < grid_size; j++) {
      auto f = frequencies(i, j);
      coord_fourier(i, j) = pow(std::norm(complex(f.real(), f.imag())), 2.) * l;
    }
  };

  // Derivative of transforme_temperature
  Matrix<complex> temperature_rate(grid_size);
  temperature_rate =
      1. / (mass * heat_capacity) *
      (transform_heatsource -
       heat_conductivity * transform_temperature * coord_fourier);

  // FFT inverse
  Matrix<complex> delta_temperature = FFT::itransform(temperature_rate);

  UInt p = 0;
  // Temperature next time-step
  for (UInt i = 0; i < grid_size; i++) {
    for (UInt j = 0; j < grid_size; j++) {
      Particle &point = system.getParticle(p);
      auto &matpoint = static_cast<MaterialPoint &>(point);
      matpoint.getHeatRate() = delta_temperature(i, j).real();
      matpoint.getTemperature() +=
          time_increment * delta_temperature(i, j).real();
      p++;
    }
  }
}
/* -------------------------------------------------------------------------- */
