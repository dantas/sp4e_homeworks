#ifndef FFT_HH
#define FFT_HH
/* ------------------------------------------------------ */
#include <fftw3.h>

#include <complex>
#include <tuple>

#include "matrix.hh"
#include "my_types.hh"
/* ------------------------------------------------------ */

struct FFT {
  static Matrix<complex> transform(Matrix<complex>& m);
  static Matrix<complex> itransform(Matrix<complex>& m);

  static Matrix<std::complex<int>> computeFrequencies(int size);
};

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::transform(Matrix<complex>& m_in) {
  fftw_plan p;

  auto in = (fftw_complex*)m_in.data();

  Matrix<complex> m_out(m_in.size());
  auto out = (fftw_complex*)m_out.data();

  p = fftw_plan_dft_2d(m_in.rows(), m_in.cols(), in, out, FFTW_FORWARD,
                       FFTW_ESTIMATE);
  fftw_execute(p);

  fftw_destroy_plan(p);

  return m_out;
}

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::itransform(Matrix<complex>& m_in) {
  fftw_plan p;

  auto in = (fftw_complex*)m_in.data();

  Matrix<complex> m_out(m_in.size());
  auto out = (fftw_complex*)m_out.data();

  p = fftw_plan_dft_2d(m_in.rows(), m_in.cols(), in, out, FFTW_BACKWARD,
                       FFTW_ESTIMATE);
  fftw_execute(p);
  m_out /= (m_in.rows() * m_in.cols());

  fftw_destroy_plan(p);

  return m_out;
}

/* ------------------------------------------------------ */

inline Matrix<std::complex<int>> FFT::computeFrequencies(int size) {
  Matrix<std::complex<int>> m_out(size);

  for (auto&& entry : index(m_out)) {
    UInt i = std::get<0>(entry);
    UInt j = std::get<1>(entry);
    auto& value = std::get<2>(entry);

    // fill frequency on x in the real part and frequency on y in the imaginary
    // part
    UInt freq_i, freq_j;
    if (size % 2 == 0) {
      freq_i = i < size / 2 ? i : i - size;
      freq_j = j < size / 2 ? j : j - size;
    } else {
      freq_i = i <= (size - 1) / 2 ? i : i - size;
      freq_j = j <= (size - 1) / 2 ? j : j - size;
    }
    value.real(freq_i);
    value.imag(freq_j);
  }
  
  return m_out;
}

#endif  // FFT_HH
