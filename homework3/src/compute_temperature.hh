#ifndef __COMPUTE_TEMPERATURE__HH__
#define __COMPUTE_TEMPERATURE__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"

//! Compute temperature at the next time-step (n+1)
class ComputeTemperature : public Compute {

  // Virtual implementation
public:
  //! Compute temperature
  void compute(System& system) override;

  void setHeatConductivity(const Real kappa){this->heat_conductivity = kappa; };
  void setHeatCapacity(const Real C){this->heat_capacity = C;};
  void setMass(const Real rho){this->mass= rho;};
  void setTimeIncrement(const Real dt){this->time_increment = dt;};
  void setDl(const Real dl){this->dl = dl;};

protected:
  Real heat_conductivity ;
  Real heat_capacity;
  Real mass;
  Real time_increment;
  Real dl;

};

/* -------------------------------------------------------------------------- */
#endif  //__COMPUTE_TEMPERATURE__HH__
