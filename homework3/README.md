# Homework 3

In this homework, we add a heat solver in the particle code. Unfortunately, we didn't manage to make the solver work properly. We provide our code as it is along with the corresponding tests. The zero temperature boundary condition is not implemented as well.

## Usage

### Build the executables

From the `homework3` directory, do:
```bash
mkdir build
cd build
ccmake ..
[ set the options that you need ]
make
```
Then, inside `build/src/`, the `particles` executable can be found along with the tests. In the same folder will also be placed a python script, `gen_heat_distr.py`, which allows to generate a heat source input.

### Generate a heat distribution

The python script `gen_heat_distr.py` can be used to generate a heat distribuion on a square domain where temperature is 1 inside a circle centered in the center of the square and 0 outside.

```bash
python3 gen_heat_distr.py size radius N filename
```

with:
* `size` (float): half length of the side of the square domain
* `radius` (float): radius of the heat distribution
* `N` (int): number of discretization point in the half length
* `filename` (string): name of the output file

For instance, to generate a heat distriution on a grid of 512x512 elements, do:
```bash
python3 gen_heat_distr.py 0.5 0.5 256 point_input.csv
```

### Solve the heat problem

Unfortunately the heat solver do not function properly as it is. The general usage would normally be:

```bash
./particles nsteps dump_freq input.csv particle_type timestep
```

with:
* `nsteps` (int): the number of simulation step
* `dump_freq` (int): the frequency at which to dump output
* `input.csv` (string): the input file containing the starting state of the particles
* `particle_type` (string): the type of particle to simulate, in this case it would be `material_point`
* `timestep` (float): the timestep of the simulation

For instance to solve the heat equation on a grid of 512 by 512 material points with the input previously generated, do:
```bash
./particles 10 1 point_input.csv material_point 0.1
```
This would run 10 iteration of the heat solver with a timestep of 0.1 and dump at every iteration.
