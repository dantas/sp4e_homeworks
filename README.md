# sp4e homeworks

This repository contains the completed homeworks for the **sp4e** class.

## Authors
**Raquel Dantas Batista** (raquel.dantasbatista@epfl.ch)  
**Shad Durussel** (shad.durussel@epfl.ch)
